const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MateriaSchema = new Schema({
  codigo: {
    type: String,
    required: true,
    unique: true,
    maxlength: 20
  },
  nombre: {
    type: String,
    required: true,
    maxlength: 255
  },
  curso: {
    type: String,
    maxlength: 2
  },
  horas: {
    type: Number
  }
})

const Materia = mongoose.model('Materia', MateriaSchema)

module.exports = Materia
