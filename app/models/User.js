const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  codigo: {
    type: Number,
    unique: true
  },
  nombre: {
    type: String,
    lowercase: true
  },
  password: { type: String, select: false }
})

const User = mongoose.model('User', UserSchema)

module.exports = User
