const express = require('express')
const router = express.Router()
const materiaController = require('../controllers/materiaController.js')

/* const auth = require('../middleware/auth')

router.use(auth.auth) */

// comento el auth, porque si lo pongo los otros dejan de funcionar
// el caso es que si pongo el auth, da el mensaje adecuado si lo haces
// sin estar logeado, por lo que funcionar, funciona, pero lo aplica
// a todos los metodos.

router.get('/', (req, res) => {
  console.log('ruta de materias')
  materiaController.index(req, res)
})

router.get('/:id', (req, res) => {
  console.log('estas en show')
  materiaController.show(req, res)
})

// descomentar el de abajo para que sea con auth, pero dejan de funcionar
// todos los demás

/* router.get('/privada', auth.auth, (req, res) => {
  console.log('ruta de materias privadas')
  materiaController.index(req, res)
}) */

router.post('/', (req, res) => {
  console.log('estas en create')
  materiaController.create(req, res)
})

router.delete('/:id', (req, res) => {
  console.log('estas en delete')
  materiaController.remove(req, res)
})

router.put('/:id', (req, res) => {
  console.log('estas en update')
  materiaController.update(req, res)
})

module.exports = router
