const servicejwt = require('../services/servicejwt')

const auth = function (req, res, next) {
  console.log(req.headers.authorization)
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'No tienes permiso' })
  }
  const token = req.headers.authorization.split(' ')[1]
  try {
    payload = servicejwt.decodeToken(token)
  } catch (error) {
    return res.status(403).send(`${error}`)
  }
  next()
}

module.exports = {
  auth
}
