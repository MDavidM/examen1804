const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

const login = (req, res) => {
  const user = User({
    nombre: req.body.name,
    password: req.body.password
  })
  user.login(err => {
    if (err) {
      res
        .status(500)
        .send({ message: `Error al hacer login con el usuario: ${err}` })
    }
    // servicejwt nos va a crear un token
    return res.status(200).send({ token: servicejwt.createToken(user) })
  })
}

module.exports = {
  login
}
